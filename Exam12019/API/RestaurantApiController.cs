﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/restaurant")]
    [ApiController]
    public class RestaurantApiController : Controller
    {
        
        private readonly RestaurantServices _RestaurantDbRedis;

        [BindProperty]
        public RestaurantViewModel Form { set; get; }
        /// <summary>
        /// Dependency  Injection
        /// </summary>
        /// <param name="bookService"></param>
        public RestaurantApiController(RestaurantServices restaurantService)
        {
            this._RestaurantDbRedis = restaurantService;
        }
        /// <summary>
        /// Api untuk menambahkan data restaurant ke Redis
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "insert-restaurant")]
        public async Task<ActionResult> Post([FromBody]RestaurantViewModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _RestaurantDbRedis.InsertRestaurantAsync(model);
            return Ok();
        }
        /// <summary>
        /// Api untuk mengupdate data restaurant berdasarkan Id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}", Name = "update-restaurant")]
        public async Task<ActionResult> Update([FromBody]RestaurantViewModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var restaurantData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findRestaurant = restaurantData.FindIndex(Q => Q.RestaurantId == id);

            if (findRestaurant == -1)
            {
                return NotFound();
            }

            restaurantData[findRestaurant] = model;

            await _RestaurantDbRedis.UpdateRestaurantAsync(restaurantData);
            return Ok();
        }
        /// <summary>
        /// Api untuk mendelete data restaurant berdasarkan Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "delete-restaurant")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var restaurantData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findRestaurant = restaurantData.FindIndex(Q => Q.RestaurantId == id);

            if (findRestaurant == -1)
            {
                return NotFound();
            }

            restaurantData.RemoveAt(findRestaurant);

            await _RestaurantDbRedis.UpdateRestaurantAsync(restaurantData);
            return Ok();
        }
        /// <summary>
        /// Api untuk mendapatkan data restaurant secara keseluruhan
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "get-restaurant")]
        public async Task<ActionResult<List<RestaurantViewModel>>> Get()
        {
            var restaurantData = await _RestaurantDbRedis.GetRestaurantAsync();
            if (restaurantData == null)
            {
                return NotFound();
            }
            return restaurantData;

        }

        /// <summary>
        /// Api untuk mendapatkan data restaurant berdasarkan Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "get-restaurant-by-id")]
        public async Task<ActionResult<RestaurantViewModel>> GetDataById(Guid id)
        {
            var restaurantData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findRestaurant = restaurantData.Where(Q => Q.RestaurantId == id).FirstOrDefault();

            if (findRestaurant == null)
            {
                return NotFound();
            }

            return findRestaurant;
        }
    }
}
