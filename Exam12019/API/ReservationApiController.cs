﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/reservation")]
    [ApiController]
    public class ReservationApiController : Controller
    {
        private readonly ReservationServices _ReservationDbRedis;
        private readonly RestaurantServices _RestaurantDbRedis;

        [BindProperty]
        public ReservationViewModel Form { set; get; }
        /// <summary>
        /// Dependency  Injection
        /// </summary>
        /// <param name="bookService"></param>
        public ReservationApiController(ReservationServices reservationService, RestaurantServices restaurantServices)
        {
            this._ReservationDbRedis = reservationService;
            this._RestaurantDbRedis = restaurantServices;
        }
        /// <summary>
        /// Api untuk menambahkan data reservation ke Redis
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "insert-reservation")]
        public async Task<ActionResult> Post([FromBody]ReservationViewModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var getRestaurants = await _RestaurantDbRedis.GetRestaurantAsync();
            var findRestaurant = getRestaurants.Where(Q => Q.RestaurantId == model.RestaurantId).Any();
            if(findRestaurant == false)
            {
                return BadRequest("Invalid Restaurant Id");
            }
            await _ReservationDbRedis.InsertReservationAsync(model);
            return Ok();
        }
        /// <summary>
        /// Api untuk mengupdate data reservation berdasarkan Id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}", Name = "update-reservation")]
        public async Task<ActionResult> Update([FromBody]ReservationViewModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var reservationData = await _ReservationDbRedis.GetReservationAsync();
            var findReservation = reservationData.FindIndex(Q => Q.ReservationId == id);

            if (findReservation == -1)
            {
                return NotFound();
            }

            reservationData[findReservation] = model;

            await _ReservationDbRedis.UpdateReservationAsync(reservationData);
            return Ok();
        }
        /// <summary>
        /// Api untuk mendelete data reservation berdasarkan Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "delete-reservation")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var reservationData = await _ReservationDbRedis.GetReservationAsync();
            var findReservation = reservationData.FindIndex(Q => Q.ReservationId == id);

            if (findReservation == -1)
            {
                return NotFound();
            }

            reservationData.RemoveAt(findReservation);

            await _ReservationDbRedis.UpdateReservationAsync(reservationData);
            return Ok();
        }
        /// <summary>
        /// Api untuk mendapatkan data reservation secara keseluruhan
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "get-reservation")]
        public async Task<ActionResult<List<ReservationViewModel>>> Get()
        {
            var reservationData = await _ReservationDbRedis.GetReservationAsync();
            if (reservationData == null)
            {
                return NotFound();
            }
            return reservationData;

        }

        /// <summary>
        /// Api untuk mendapatkan data reservation berdasarkan Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "get-reservation-by-id")]
        public async Task<ActionResult<ReservationViewModel>> GetDataById(Guid id)
        {
            var reservationData = await _ReservationDbRedis.GetReservationAsync();
            var findReservation = reservationData.Where(Q => Q.ReservationId == id).FirstOrDefault();

            if (findReservation == null)
            {
                return NotFound();
            }

            return findReservation;
        }
    }
}
