﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public IndexModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }

        [BindProperty]
        public List<RestaurantViewModel> AllRestaurant { set; get; }
        [BindProperty]
        public ReservationFormModel Form { get; set; }
        /// <summary>
        /// Untuk mengambil data restaurant
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var client = _HttpFact.CreateClient();
            var respond = await client.GetAsync(apiUrl);
            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("No Restaurant Data Found");
            }
            AllRestaurant = await respond.Content.ReadAsAsync<List<RestaurantViewModel>>();
            return Page();
        }

        /// <summary>
        /// Untuk memasukan data reservasi sesuai dengan data
        /// yang telah diinput pada form
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if(Form == null)
            {
                return Page();
            }

            var apiUrl = "http://localhost:50508/api/reservation/";
            var client = _HttpFact.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new ReservationViewModel
            {
                ReservationId = Guid.NewGuid(),
                Name = Form.Name,
                Email = Form.Email,
                RestaurantId = Form.RestaurantId,
                ReservationTime = Form.ReservationTime
            });

            if (response.IsSuccessStatusCode == false)
            {
                return BadRequest("Invalid Restaurant ID");
            }

            return RedirectToPage("./Index");

        }

        /// <summary>
        /// Model untuk menampung data dari reservation model
        /// </summary>
        public class ReservationFormModel
        {
            [Required]
            [StringLength(255)]
            public string Name { get; set; }
            [Required]
            [StringLength(255)]
            [EmailAddress]
            public string Email { get; set; }
            [Required]
            public DateTime ReservationTime { get; set; }
            [Required]
            public Guid RestaurantId { get; set; }
        }
    }
}
