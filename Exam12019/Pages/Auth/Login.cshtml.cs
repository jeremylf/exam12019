using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

namespace Exam12019.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly IOptions<AppSettings> _Options;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="options"></param>
        public LoginModel(IOptions<AppSettings> options)
        {
            this._Options = options;
        }

        public void OnGet()
        {
        }
        [BindProperty]
        public LoginFormModel Form { set; get; }

        /// <summary>
        /// Untuk memastikan bahwa username dan password sesuai
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }
            var username = _Options.Value.Accelist;
            var password = _Options.Value.AdminPassword;
            var valid = Form.Username == username && BCrypt.Net.BCrypt.Verify(Form.Password, password);
            if(valid == false)
            {
                ModelState.AddModelError("Form.Password", "Invalid login username or password");
                return Page();
            }
            var id = new ClaimsIdentity("ClaimIdentityLogin");
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, _Options.Value.Accelist));
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("ClaimIdentityLogin", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });
            return RedirectToPage("/Restaurant/Index");
        }

        /// <summary>
        /// Untuk menampung data login yang diinput user
        /// </summary>
        public class LoginFormModel
        {
            [Required]
            public string Username { get; set; }

            [Required]
            public string Password { get; set; }
        }
    }
}