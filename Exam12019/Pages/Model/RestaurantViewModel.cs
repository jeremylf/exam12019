﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Pages.Model
{
    /// <summary>
    /// Model class restaurant
    /// </summary>
    public class RestaurantViewModel
    {
        /// <summary>
        /// Id dari sebuah restaurant
        /// </summary>
        public Guid RestaurantId { get; set; }
        /// <summary>
        /// Nama dari sebuah restaurant
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Alamat dari sebuah restaurant
        /// </summary>
        public string Address { get; set; }
    }
}
