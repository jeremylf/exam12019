﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Pages.Model
{
    /// <summary>
    /// Model untuk reservasi
    /// </summary>
    public class ReservationViewModel
    {
        /// <summary>
        /// Id reservasi
        /// </summary>
        public Guid ReservationId { set; get; }
        /// <summary>
        /// Nama orang yang melakukan reservasi
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Email orang yang melakukan reservasi
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Nama restaurant yang direservasi
        /// </summary>
        public Guid RestaurantId { get; set; }
        /// <summary>
        /// Alamat restaurant yang direservasi
        /// </summary>
        public DateTime ReservationTime { get; set; }
    }
}
