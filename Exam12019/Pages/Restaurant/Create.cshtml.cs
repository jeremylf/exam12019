using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class CreateModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public CreateModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }

        public void OnGet()
        {
        }

        [BindProperty]
        public RestaurantFormModel Form { set; get; }
        /// <summary>
        /// Untuk memasukkan data ke dalam Redis melalui Api
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if (Form == null)
            {
                return Page();
            }
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var client = _HttpFact.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantViewModel
            {
                RestaurantId = Guid.NewGuid(),
                Name = Form.Name,
                Address = Form.Address
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Insert via API Failed");
            }

            return RedirectToPage("./Index");

        }
        /// <summary>
        /// Model untuk menampung data restaurant
        /// </summary>
        public class RestaurantFormModel
        {
            [Required]
            [StringLength(255)]
            public string Name { get; set; }
            [Required]
            [StringLength(2000)]
            public string Address { get; set; }
        }

    }
}