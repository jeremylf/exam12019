using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public IndexModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }
        [BindProperty]
        public List<RestaurantViewModel> Form { set; get; }
        /// <summary>
        /// Untuk mengambil data restaurant
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var client = _HttpFact.CreateClient();
            var respond = await client.GetAsync(apiUrl);
            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("No Restaurant Data Found");
            }
            Form = await respond.Content.ReadAsAsync<List<RestaurantViewModel>>();
        }
    }
}