using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class DeleteModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;

        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public DeleteModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }
        /// <summary>
        /// Mendapatkan Id
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public Guid Id { set; get; }
        /// <summary>
        /// Nama Restaurant yang ingin didelete
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public string Name { get; set; }

        /// <summary>
        /// Untuk mendapatkan data berdasarkan Id yang ingin di delete
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + Id;
            var client = _HttpFact.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Restaurant Id Not Found");
            }

            var content = await respond.Content.ReadAsAsync<RestaurantViewModel>();
            Name = content.Name;
            return Page();
        }
        /// <summary>
        /// Untuk menghapus data yang dipilih
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + Id;
            var client = _HttpFact.CreateClient();
            var response = await client.DeleteAsync(apiUrl);
            return RedirectToPage("./Index");
        }
    }
}