using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class UpdateModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public UpdateModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }

        [BindProperty]
        public RestaurantFormModel Form { set; get; }

        [BindProperty(SupportsGet = true)]
        public Guid Id { set; get; }
        /// <summary>
        /// Untuk mengambil data restaurant dari Redis
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + Id;
            var client = _HttpFact.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get by ID via API Failed");
            }

            var content = await respond.Content.ReadAsAsync<RestaurantViewModel>();
            Form = new RestaurantFormModel
            {
                Name = content.Name,
                Address = content.Address
            };

            return Page();
        }

        /// <summary>
        /// Untuk mengirim data restaurant yang akan diproses(update) ke Api
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            if(Form == null)
            {
                return Page();
            }
            var apiUrl = "http://localhost:50508/api/restaurant/" + Id;
            var client = _HttpFact.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantViewModel
            {
                RestaurantId = Id,
                Name = Form.Name,
                Address = Form.Address
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Update via API Failed");
            }

            return RedirectToPage("./Index");
        }
        /// <summary>
        /// Model sementara untuk menampung data restaurant berdasarkan Id
        /// </summary>
        public class RestaurantFormModel
        {
            [Required]
            [StringLength(255)]
            public string Name { get; set; }
            [Required]
            [StringLength(2000)]
            public string Address { get; set; }
        }
    }
}