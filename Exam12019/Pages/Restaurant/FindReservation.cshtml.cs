using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    public class FindReservationModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFact;

        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public FindReservationModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFact = httpClientFactory;
        }

        [BindProperty(SupportsGet = true)]
        [Required]
        public Guid Id { set; get; }

        [BindProperty(SupportsGet = true)]
        public List<ReservationFormModel> Form { set; get; }

        /// <summary>
        /// Untuk mendapatkan data reservasi sesuai dengan id
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            if (Id == null)
            {
                var apiUrlReservation = "http://localhost:50508/api/reservation/" + Id;
                var apiUrlRestaurant = "http://localhost:50508/api/restaurant/" + Id;
                var client = _HttpFact.CreateClient();
                var respondReservation = await client.GetAsync(apiUrlReservation);
                var respondRestaurant = await client.GetAsync(apiUrlRestaurant);

                if (respondReservation.IsSuccessStatusCode == false)
                {
                    throw new Exception("Reservation Id Not Found");
                }

                if (respondRestaurant.IsSuccessStatusCode == false)
                {
                    throw new Exception("Restaurant Id Not Found");
                }

                var contentReservation = await respondReservation.Content.ReadAsAsync<List<ReservationViewModel>>();
                var contentRestaurant = await respondReservation.Content.ReadAsAsync<List<RestaurantViewModel>>();
                if (contentReservation == null)
                {
                    Form = new List<ReservationFormModel>();
                    return Page();
                }
                foreach (var item in contentReservation)
                {
                    Form = new List<ReservationFormModel>();
                    Form.Add(new ReservationFormModel
                    {
                        Name = item.Name,
                        Email = item.Email,
                        RestaurantName = contentRestaurant
                        .Where(Q => Q.RestaurantId.ToString() == item.RestaurantId.ToString())
                        .FirstOrDefault().Name.ToString(),
                        RestaurantAddress = contentRestaurant
                        .Where(Q => Q.RestaurantId.ToString() == item.RestaurantId.ToString())
                        .FirstOrDefault().Address.ToString()
                    });
                }

                return Page();
            }
            return Page();
        }


        /// <summary>
        /// Untuk menampilkan data reservasi sesuai dengan id
        /// yang telah diinput
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrlReservation = "http://localhost:50508/api/reservation/";
            var apiUrlRestaurant = "http://localhost:50508/api/restaurant/";
            var client = _HttpFact.CreateClient();
            var respondReservation = await client.GetAsync(apiUrlReservation);
            var respondRestaurant = await client.GetAsync(apiUrlRestaurant);

            if (respondReservation.IsSuccessStatusCode == false)
            {
                throw new Exception("Reservation Id Not Found");
            }

            if (respondRestaurant.IsSuccessStatusCode == false)
            {
                throw new Exception("Restaurant Id Not Found");
            }

            var contentReservation = await respondReservation.Content.ReadAsAsync<List<ReservationViewModel>>();
            var contentRestaurant = await respondReservation.Content.ReadAsAsync<List<RestaurantViewModel>>();

            var findReservation = contentReservation.Where(Q => Q.ReservationId == Id).FirstOrDefault();
            //Form.Clear();
            Form.Add(new ReservationFormModel
            {
                Name = findReservation.Name,
                Email = findReservation.Email,
                RestaurantName = contentRestaurant
                    .Where(Q => Q.RestaurantId.ToString() == findReservation.RestaurantId.ToString())
                    .FirstOrDefault().Name.ToString(),
                RestaurantAddress = contentRestaurant
                    .Where(Q => Q.RestaurantId.ToString() == findReservation.RestaurantId.ToString())
                    .FirstOrDefault().Address.ToString()
            });

            return Page();
        }


        /// <summary>
        /// Model untuk menampung reservation yang akan ditampilkan
        /// </summary>
        public class ReservationFormModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public string RestaurantName { get; set; }
            public string RestaurantAddress { get; set; }
        }
    }
}