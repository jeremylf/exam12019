﻿using Exam12019.Pages.Model;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class RestaurantServices
    {
        private readonly IDistributedCache _Cache;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="distributedCache"></param>
        public RestaurantServices(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }
        /// <summary>
        /// Untuk menambahkan data restaurant ke Redis
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task InsertRestaurantAsync(RestaurantViewModel restaurant)
        {
            var restaurants = await GetRestaurantAsync();
            if (restaurants == null)
            {
                restaurants = new List<RestaurantViewModel>();
            }

            restaurants.Add(restaurant);
            var restaurantJson = JsonConvert.SerializeObject(restaurants);
            await _Cache.SetStringAsync("Restaurants", restaurantJson);
        }
        /// <summary>
        /// Untuk mendapatkan data restaurant dari Redis
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantViewModel>> GetRestaurantAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Restaurants");
            if (valueJson == null)
            {
                return new List<RestaurantViewModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantViewModel>>(valueJson);
        }
        /// <summary>
        /// Untuk mengupdate data restaurant yang ada di Redis
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task UpdateRestaurantAsync(List<RestaurantViewModel> restaurant)
        {
            var restaurantJson = JsonConvert.SerializeObject(restaurant);
            await _Cache.SetStringAsync("Restaurants", restaurantJson);
        }
    }
}
