﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class AppSettings
    {
        public string Accelist { get; set; }
        public string AdminPassword { get; set; }
        public string ReservationApiKey { get; set; }

    }
}
