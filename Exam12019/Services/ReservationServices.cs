﻿using Exam12019.Pages.Model;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class ReservationServices
    {
        private readonly IDistributedCache _Cache;
        /// <summary>
        /// Dependency Injection
        /// </summary>
        /// <param name="distributedCache"></param>
        public ReservationServices(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }
        /// <summary>
        /// Untuk menambahkan data reservation ke Redis
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public async Task InsertReservationAsync(ReservationViewModel reservation)
        {
            var reservations = await GetReservationAsync();
            if (reservations == null)
            {
                reservations = new List<ReservationViewModel>();
            }

            reservations.Add(reservation);
            var reservationJson = JsonConvert.SerializeObject(reservations);
            await _Cache.SetStringAsync("Reservations", reservationJson);
        }
        /// <summary>
        /// Untuk mendapatkan data reservation dari Redis
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReservationViewModel>> GetReservationAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Reservations");
            if (valueJson == null)
            {
                return new List<ReservationViewModel>();
            }
            return JsonConvert.DeserializeObject<List<ReservationViewModel>>(valueJson);
        }
        /// <summary>
        /// Untuk mengupdate data reservation yang ada di Redis
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        public async Task UpdateReservationAsync(List<ReservationViewModel> reservation)
        {
            var reservationJson = JsonConvert.SerializeObject(reservation);
            await _Cache.SetStringAsync("Reservations", reservationJson);
        }
    }
}
